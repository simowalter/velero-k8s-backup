# velero-k8s-backup



## Getting started

1. Download binary
```bash
wget https://github.com/heptio/velero/releases/download/v1.12.0/velero-v1.12.0-linux-amd64.tar.gz
tar zxf velero-v1.12.0-linux-amd64.tar.gz
sudo mv velero-v1.12.0-linux-amd64/velero /usr/local/bin/
rm -rf velero*
```

2. Check verion
```bash
velero version
```

3. Configure the credentials of the remote object storage where to store backups
```bash
cat <<EOF>> minio.credentials
[default]
aws_access_key_id=xxx
aws_secret_access_key=xxxxxxxx
EOF
```

4. Deploy Velero on the cluster
```bash
velero install    --provider aws --plugins velero/velero-plugin-for-aws   --bucket bucket-velero    --secret-file ./minio.credentials    --backup-location-config region=main,s3ForcePathStyle=true,s3Url=http://<IP-SERVER-S3>:9000
```

5. Check the deployment status
```bash
kubectl get all -n velero
kubectl logs deployment/velero -n velero
velero version
```

6. Enable Auto Completion (tab completion)
```bash
velero completion bash > velero.autocompletion
source velero.autocompletion
```

## CLI

7. Backup specific namespace
```bash
velero backup create backup-ns-myapp --include-namespaces ns-myapp

velero backup create backup-entire-cluster-except-pv --exclude-resources persistentvolumes
```

Backup of the entire cluster
```bash
velero backup create backup-entire-cluster
```

8. restore the backup
```bash
velero restore create restore-ns-myapp --from-backup backup-ns-myapp
```

9. Verify the restoration
```bash
velero restore describe restore-ns-myapp
velero restore logs restore-ns-myapp
```

10. Delete the restore backup
```bash
velero restore delete restore-ns-myapp
```

Delete all backups
```bash
velero backup delete --all
```

11. Scheduled backup (every week) (with retention of 2 weeks)
```bash
velero schedule create scheduled-backup-ns-carousel --schedule="@every 1w" --ttl 2w --include-namespaces carousel
```

12. Get schedule
```bash
velero schedule get
```